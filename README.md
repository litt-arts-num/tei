
# Déployement sur Stendhal : en static
~/.config/composer/vendor/bin/daux --destination=static --source=.
puis scp -r static/ stendhal.msh-alpes.fr:/var/www/stendhal/tei

# En local
~/.config/composer/vendor/bin/daux generate -c config.json -s .
~/.config/composer/vendor/bin/daux serve -c config.json -s .

# Tester la page http://localhost:8085/index

# Markdown étendu
composer require michelf/php-markdown
composer install
require 'vendor/autoload.php';



