<strong>Cette rubrique <a href="fr/00_Transcrire_Kesako">Transcrire kesako ?</a></strong> est une introduction à la problématique de la transcription. Sans constituer un article scientifique, nous y soulevons les éléments qui nous semblent clefs lorsque l'on débute un projet de transcription&nbsp;:</p>
     <ul>
	<li>qu'est-ce que transcrire&nbsp;?</li>
	<li> pourquoi transcrire&nbsp;?</li>
	<li>que transcrire&nbsp;?</li>
	<li>comment transcrire&nbsp;?</li>
     </ul>

