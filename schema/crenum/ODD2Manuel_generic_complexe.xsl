<?xml version="1.0" encoding="UTF-8"?>
<!--
/**
* XSLT for generating HTML manual from ODD file
* version 2023-03-27
* @author AnneGF@CNRS
* @date : 2022-2023
**/
-->
<!DOCTYPE mon-document [
  <!ENTITY times "&#215;">
  <!ENTITY nbsp "&#160;">
]>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" exclude-result-prefixes="xs tei sch" version="2.0"
    xmlns:sch="http://purl.oclc.org/dsdl/schematron" xmlns:tei="http://www.tei-c.org/ns/1.0">

    <xsl:output method="html" omit-xml-declaration="yes" indent="yes"/>
    <xsl:strip-space elements="*"/>
    <xsl:variable name="STAND-ALONE">1</xsl:variable>
    <!--<xsl:preserve-space elements=""/>-->

    <xsl:template match="/tei:TEI">
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;html lang="fr"></xsl:text>
            <head>
                <!-- Required meta tags -->
                <meta charset="utf-8"/>
                <meta name="viewport" content="width=device-width, initial-scale=1"/>
                <!-- Bootstrap CSS -->
                <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css"
                    rel="stylesheet"
                    integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC"
                    crossorigin="anonymous"/>
                <link href="style.css" rel="stylesheet"/>

                <title>Manuel généré depuis ODD - <xsl:value-of
                        select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/></title>
            </head>
            <xsl:text disable-output-escaping="yes">&lt;body></xsl:text>
        </xsl:if>
        <xsl:for-each select="//tei:elementSpec/@ident">
            <xsl:variable name="elem">
                <xsl:text> </xsl:text>
                <xsl:value-of select="."/>
                <xsl:text> </xsl:text>
            </xsl:variable>
            <xsl:variable name="list">
                <xsl:text> </xsl:text>
                <xsl:for-each select="//tei:moduleRef/@include/tokenize(., ' ')">
                    <xsl:text> </xsl:text>
                    <xsl:value-of select="."/>
                    <xsl:text> </xsl:text>
                </xsl:for-each>
                <xsl:text> </xsl:text>
            </xsl:variable>
            <xsl:if test="not(matches($list, $elem))">
                <div class="btn-danger btn-sm">
                    <xsl:text>L'élément </xsl:text>
                    <code class="fw-bold text-primary">
                        <xsl:value-of select="$elem"/>
                    </code>
                    <xsl:text> est utilisé sans être inclu dans aucun module. Veuillez l'ajouter dans l'attribut </xsl:text>
                    <code class="fw-bold text-primary">@include</code>
                    <xsl:text> du module auquel il appartient (cf. </xsl:text>
                    <a target="_blank">
                        <xsl:attribute name="href">
                            <xsl:text>https://tei-c.org/release/doc/tei-p5-doc/fr/html/ref-</xsl:text>
                            <xsl:value-of select="$elem"/>
                            <xsl:text>.html</xsl:text>
                        </xsl:attribute>
                        <xsl:text>doc de la TEI</xsl:text>
                    </a>
                    <xsl:text>).</xsl:text>
                </div>
            </xsl:if>
        </xsl:for-each>
        <h1 class="display-4">
            <xsl:value-of select="tei:teiHeader/tei:fileDesc/tei:titleStmt/tei:title"/>
        </h1>
        <xsl:apply-templates select="tei:text/tei:body/*"/>
        <hr/>
        <h3>Remarques générales sur le modèle TEI et son manuel</h3>
        <ul>
            <li>
                <xsl:text>Modules et éléments utilisés&nbsp;:</xsl:text>
                <ul>
                    <xsl:apply-templates select="//tei:moduleRef" mode="information"/>
                </ul>
            </li>
            <xsl:if test="count(//tei:classRef) > 0">
                <li>
                    <xsl:text>Classes utilisées&nbsp;:</xsl:text>
                    <ul>
                        <xsl:apply-templates select="//tei:classRef" mode="information"/>
                    </ul>
                </li>
            </xsl:if>
            <li>
                <xsl:text>Classes modifiées&nbsp;:</xsl:text>
                <ul>
                    <xsl:apply-templates select="//tei:classSpec[not(@mode = 'delete')]"
                        mode="information"/>
                </ul>
            </li>
            <li>
                <xsl:text>Classes supprimées&nbsp;:</xsl:text>
                <ul>
                    <li>
                        <xsl:for-each select="//tei:classSpec[@mode = 'delete']">
                            <xsl:apply-templates select="." mode="information"/>
                            <xsl:if test="position() != last()">
                                <xsl:text>, </xsl:text>
                            </xsl:if>
                        </xsl:for-each>
                    </li>
                </ul>
            </li>
            <xsl:for-each-group select="//tei:elementSpec/@ident" group-by=".">
                <xsl:if test="count(current-group()) > 1">
                    <li class="warning btn-warning btn-sm">
                        <xsl:text>L'élément </xsl:text>
                        <code>
                            <xsl:value-of select="current-grouping-key()"/>
                        </code>
                        <xsl:text> est spécifié plusieurs fois.</xsl:text>
                    </li>
                </xsl:if>
            </xsl:for-each-group>

            <xsl:variable name="tei" select="/tei:TEI"/>
            <xsl:for-each select="//tei:moduleRef/tokenize(@include, ' ')">
                <xsl:if test="not($tei//tei:elementSpec[@ident = current()])">
                    <li class="warning btn-warning btn-sm">
                        <xsl:text>L'élément </xsl:text>
                        <code>
                            <xsl:value-of select="."/>
                        </code>
                        <xsl:text> est utilisé mais pas documenté.</xsl:text>
                    </li>
                </xsl:if>
            </xsl:for-each>
        </ul>
        <xsl:if test="$STAND-ALONE">
            <xsl:text disable-output-escaping="yes">&lt;/body></xsl:text>
            <xsl:text disable-output-escaping="yes">&lt;/html></xsl:text>
        </xsl:if>
    </xsl:template>

    <xsl:template match="tei:head">
        <!-- Niveau du titre donné selon la "distance" à l'élément <body/> -->
        <xsl:variable name="level" select="count(ancestor-or-self::*[ancestor::tei:body]) + 1"/>
        <xsl:element name="h{$level}">
            <xsl:if test="not(@xml:id = '')">
                <xsl:attribute name="id">
                    <xsl:value-of select="@xml:id"/>
                </xsl:attribute>
            </xsl:if>
            <xsl:apply-templates/>
        </xsl:element>
    </xsl:template>
    <xsl:template match="tei:list">
        <ul>
            <xsl:apply-templates/>
        </ul>
    </xsl:template>
    <xsl:template match="tei:item">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:gi">
        <code>
            <xsl:text>&lt;</xsl:text>
            <xsl:value-of select="."/>
            <xsl:text>&gt;</xsl:text>
        </code>
    </xsl:template>
    <xsl:template match="tei:schemaSpec"/>
    <xsl:template match="tei:moduleRef"/>
    <xsl:template match="tei:moduleRef" mode="information" name="module_info">
        <li>
            <xsl:text>Module </xsl:text>
            <i>
                <xsl:value-of select="@key"/>
            </i>
            <xsl:if test="@include">
                <xsl:text> (élément</xsl:text>
                <xsl:if test="contains(@include, ' ')">
                    <xsl:text>s</xsl:text>
                </xsl:if>
                <xsl:text> </xsl:text>
                <xsl:for-each select="tokenize(@include, ' ')">
                    <code>
                        <xsl:value-of select="."/>
                    </code>
                    <xsl:choose>
                        <xsl:when test="position() lt last() - 1">
                            <xsl:text>, </xsl:text>
                        </xsl:when>
                        <xsl:when test="position() eq (last() - 1)">
                            <xsl:text> et </xsl:text>
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
            </xsl:if>
            <xsl:if test="@exclude">
                <xsl:text> (sauf </xsl:text>
                <xsl:choose>
                    <xsl:when test="contains(@include, ' ')">
                        <xsl:text>les éléments</xsl:text>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:text>l'élément</xsl:text>
                    </xsl:otherwise>
                </xsl:choose>
                <xsl:text> </xsl:text>
                <xsl:for-each select="tokenize(@include, ' ')">
                    <code>
                        <xsl:value-of select="."/>
                    </code>
                    <xsl:choose>
                        <xsl:when test="position() lt last() - 1">
                            <xsl:text>, </xsl:text>
                        </xsl:when>
                        <xsl:when test="position() eq (last() - 1)">
                            <xsl:text> et </xsl:text>
                        </xsl:when>
                        <xsl:otherwise/>
                    </xsl:choose>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
            </xsl:if>
        </li>
    </xsl:template>
    <xsl:template match="tei:specGrp">
        <div><xsl:attribute name="id">
            <xsl:value-of select="./@xml:id"/>
        </xsl:attribute>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="tei:specGrpRef">
        <div>
            <xsl:variable name="id">
                <xsl:value-of select="substring-after(@target,'#')"/>
            </xsl:variable>
            <xsl:attribute name="href">
            <xsl:value-of select="./@target"/>
        </xsl:attribute>
        <xsl:apply-templates select="//tei:specGrp[@xml:id = $id]"/>
        </div>
    </xsl:template>
    <xsl:template match="tei:elementSpec">
        <h4><xsl:attribute name="id">
                <xsl:value-of select="./@xml:id"/>
            </xsl:attribute> Élément <code><xsl:value-of select="@ident"/></code></h4>
        <xsl:if
            test="not(tei:desc) and not(tei:exemplum) and not(tei:remarks) and not(tei:constraintSpec)">
            <div class="warning btn-warning btn-sm">! Cet élément mériterait d'être mieux
                documenté.</div>
        </xsl:if>
        <xsl:apply-templates/>
        <xsl:if test="tei:constraintSpec">
            <xsl:text>Contrainte</xsl:text>
            <xsl:if test="count(tei:constraintSpec) > 1">
                <xsl:text>s</xsl:text>
            </xsl:if>
            <xsl:text>&nbsp;:</xsl:text>
            <ul>
                <xsl:apply-templates select="tei:constraintSpec" mode="show-constraints"/>
            </ul>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:exemplum">
        <xsl:text>Exemple d'utilisation&nbsp;:</xsl:text>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:attList">
        <xsl:if test="count(tei:attDef[not(@mode = 'delete')]) > 0">
            <xsl:text>Attribut</xsl:text>
            <xsl:if test="count(tei:attDef[not(@mode = 'delete')]) > 1">
                <xsl:text>s</xsl:text>
            </xsl:if>
            <xsl:text>&nbsp;:</xsl:text>
            <ul>
                <xsl:apply-templates/>
            </ul>
        </xsl:if>
        <!-- TODO : complete with others @mode -->
    </xsl:template>
    <xsl:template match="tei:attDef">
        <xsl:if test="not(@mode = 'delete')">
            <li>
                <code>@<xsl:value-of select="@ident"/></code>&nbsp; <i><xsl:apply-templates
                        select="tei:desc" mode="inline"/></i>
                <xsl:if test="tei:datatype">
                    <xsl:apply-templates select="tei:datatype"/>
                </xsl:if>
                <xsl:if test="tei:valList">
                    <xsl:apply-templates select="tei:valList"/>
                </xsl:if>
                <xsl:apply-templates
                    select="child::*[not(name(.) = 'valList') and not(name(.) = 'desc') and not(name(.) = 'datatype')]"
                />
            </li>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:datatype">
        <!--<xsl:text> (de type&nbsp;: </xsl:text>
        <a>
            <xsl:attribute name="href">
                <xsl:text>https://www.tei-c.org/release/doc/tei-p5-doc/fr/html/ref-</xsl:text>
                <xsl:value-of select="tei:dataRef/@key"/>
                <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="tei:dataRef/@key"/>
        </a>-->
        <xsl:if test="@minOccurs or @maxOccurs">
            <!-- <xsl:text> - </xsl:text> -->
            <xsl:text> (</xsl:text>
            <xsl:choose>
                <xsl:when test="@minOccurs > 0 and (not(@maxOccurs) or @maxOccurs = 'unbounded')">
                    <xsl:text>au moins </xsl:text>
                    <xsl:value-of select="@minOccurs"/>
                    <xsl:text> valeur</xsl:text>
                    <xsl:if test="@minOccurs > 1">
                        <xsl:text>s</xsl:text>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="@minOccurs > 0">
                    <xsl:value-of select="@minOccurs"/>
                    <xsl:text> à </xsl:text>
                    <xsl:value-of select="@maxOccurs"/>
                    <xsl:text> valeur</xsl:text>
                    <xsl:if test="@maxOccurs > 1">
                        <xsl:text>s</xsl:text>
                    </xsl:if>
                </xsl:when>
                <xsl:when test="not(@maxOccurs = 'unbounded')">
                    <xsl:text>au plus </xsl:text>
                    <xsl:value-of select="@maxOccurs"/>
                    <xsl:text> valeur</xsl:text>
                    <xsl:if test="@maxOccurs > 1">
                        <xsl:text>s</xsl:text>
                    </xsl:if>
                    <xsl:text>possible</xsl:text>
                    <xsl:if test="@maxOccurs > 1">
                        <xsl:text>s</xsl:text>
                    </xsl:if>
                    <xsl:text>s</xsl:text>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:text>plusieurs valeurs possibles</xsl:text>
                </xsl:otherwise>
            </xsl:choose>
            <xsl:text>)</xsl:text>
        </xsl:if>
    </xsl:template>
    <xsl:template match="tei:desc" mode="inline">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:desc" mode="inline-parenthesis">
        <xsl:value-of
            select="replace(concat(lower-case(substring(., 1, 1)), substring(., 2)), '.$', '')"/>
    </xsl:template>
    <xsl:template match="tei:desc">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:att">
        <code>
            <xsl:text>@</xsl:text>
            <xsl:apply-templates/>
        </code>
    </xsl:template>
    <xsl:template match="tei:valList">
        <!-- @AGF : pour supprimer des listes dans le manuel mais les laisser dans le .rng :
            <xsl:choose>
             <xsl:when test="@style = 'display:none'">
                 <xsl:apply-templates/>                
             </xsl:when>
             <xsl:otherwise>
                 <ul>
                 <xsl:choose>
                     <xsl:when test="count(tei:valItem) &lt;= 5">
                         <xsl:for-each select="tei:valItem">
                             <xsl:sort select="@ident" lang="fr"/>
                             <xsl:apply-templates select="."/>
                         </xsl:for-each>
                     </xsl:when>
                     <xsl:otherwise>
                         <xsl:for-each select="tei:valItem">
                             <xsl:sort select="@ident" lang="fr"/>
                             <xsl:apply-templates select="." mode="inline"/>
                             <xsl:if test="position() &lt; last()">
                                 <xsl:text>, </xsl:text>
                             </xsl:if>
                         </xsl:for-each>
                     </xsl:otherwise>
                 </xsl:choose>
                </ul>
             </xsl:otherwise>
            </xsl:choose>? -->
        <ul>
            <xsl:choose>
                <xsl:when test="count(tei:valItem) &lt;= 5">
                    <xsl:for-each select="tei:valItem">
                        <xsl:sort select="@ident" lang="fr"/>
                        <xsl:apply-templates select="."/>
                    </xsl:for-each>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:for-each select="tei:valItem">
                        <xsl:sort select="@ident" lang="fr"/>
                        <xsl:apply-templates select="." mode="inline"/>
                        <xsl:if test="position() &lt; last()">
                            <xsl:text>, </xsl:text>
                        </xsl:if>
                    </xsl:for-each>
                </xsl:otherwise>
            </xsl:choose>
        </ul>
    </xsl:template>
    <xsl:template match="tei:valItem">
        <li>
            <code>
                <xsl:value-of select="@ident"/>
            </code>
            <xsl:if test="not(. = '')">&nbsp;: <xsl:apply-templates mode="inline"/></xsl:if>
        </li>
    </xsl:template>
    <xsl:template match="tei:valItem" mode="inline">
        <code>
            <xsl:value-of select="@ident"/>
        </code>
        <xsl:if test="not(. = '')">&nbsp;<small>(<xsl:apply-templates mode="inline-parenthesis"
                />)</small></xsl:if>
    </xsl:template>

    <xsl:template match="tei:eg">
        <pre><xsl:apply-templates/></pre>
    </xsl:template>
    <xsl:template match="tei:remarks">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:p">
        <xsl:choose>
            <xsl:when test="@style = 'example'">
                <xsl:text>Exemple d'utilisation&nbsp;:</xsl:text>
                <pre><xsl:apply-templates/></pre>
            </xsl:when>
            <xsl:otherwise>
                <p>
                    <xsl:apply-templates/>
                </p>
            </xsl:otherwise>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:hi">
        <xsl:choose>
            <xsl:when test="@rend = 'italic'">
                <i>
                    <xsl:apply-templates/>
                </i>
            </xsl:when>
            <xsl:when test="@rend = 'bold'">
                <b>
                    <xsl:apply-templates/>
                </b>
            </xsl:when>
            <xsl:when test="@rend = 'strong'">
                <strong>
                    <xsl:apply-templates/>
                </strong>
            </xsl:when>
        </xsl:choose>
    </xsl:template>
    <xsl:template match="tei:constraintSpec"/>
    <xsl:template match="tei:constraintSpec" mode="show-constraints">
        <li>
            <xsl:apply-templates/>
        </li>
    </xsl:template>
    <xsl:template match="tei:constraint">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="sch:name">
        <code>
            <xsl:value-of select="ancestor::tei:elementSpec/@ident"/>
        </code>
    </xsl:template>
    <xsl:template match="tei:content"/>
    <xsl:template match="tei:classRef"/>
    <xsl:template match="tei:classRef" mode="information">
        <li style="display: inline list-item;">
            <a>
                <xsl:attribute name="href">
                    <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                    <xsl:value-of select="@key"/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="@key"/>
            </a>
        </li>
    </xsl:template>
    <xsl:template match="tei:classSpec"/>
    <xsl:template match="tei:classSpec[not(@mode = 'delete')]" mode="information">
        <li>
            <a>
                <xsl:attribute name="href">
                    <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                    <xsl:value-of select="@ident"/>
                    <xsl:text>.html</xsl:text>
                </xsl:attribute>
                <xsl:value-of select="@ident"/>
            </a>
            <xsl:text>&nbsp;: </xsl:text>
            <xsl:for-each-group select="tei:attList/tei:attDef" group-by="@mode">
                <xsl:choose>
                    <xsl:when test="@mode = 'delete'">attributs supprimés&nbsp;(</xsl:when>
                    <xsl:otherwise>@mode '<xsl:value-of select="@mode"/>'&nbsp;(</xsl:otherwise>
                </xsl:choose>
                <xsl:for-each select="current-group()">
                    <code>
                        <xsl:text>@</xsl:text>
                        <xsl:value-of select="@ident"/>
                    </code>
                    <xsl:if test="not(position() = last())">, </xsl:if>
                </xsl:for-each>
                <xsl:text>)</xsl:text>
                <xsl:if test="not(position() = last())">, </xsl:if>
            </xsl:for-each-group>
        </li>
    </xsl:template>
    <xsl:template match="tei:classSpec[@mode = 'delete']" mode="information">
        <a>
            <xsl:attribute name="href">
                <xsl:text>https://www.tei-c.org/Vault/P5/2.0.2/doc/tei-p5-doc/en/html/ref-</xsl:text>
                <xsl:value-of select="@ident"/>
                <xsl:text>.html</xsl:text>
            </xsl:attribute>
            <xsl:value-of select="@ident"/>
        </a>
    </xsl:template>
    <xsl:template match="tei:ref">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="./@target"/>
            </xsl:attribute>
            <xsl:apply-templates/>
        </a>
    </xsl:template>
    <xsl:template match="tei:lb">
        <br/>
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:div">
        <div>
            <xsl:apply-templates/>
        </div>
    </xsl:template>
    <xsl:template match="tei:title">
        <mark>
            <xsl:apply-templates/>
        </mark>
    </xsl:template>
    <xsl:template match="tei:floatingText">
        <bloc>
            <xsl:apply-templates/>
        </bloc>
    </xsl:template>
    <xsl:template match="tei:floatingText/tei:body">
        <bloc>
            <xsl:apply-templates/>
        </bloc>
    </xsl:template>
    <xsl:template match="tei:figure">
        <xsl:apply-templates/>
    </xsl:template>
    <xsl:template match="tei:figDesc">
        <p>
            <xsl:apply-templates/>
        </p>
    </xsl:template>
    <xsl:template match="tei:graphic">
        <img>
            <xsl:attribute name="alt">
                <xsl:value-of select="'image'"/>
            </xsl:attribute>
            <xsl:attribute name="src">
                <xsl:value-of select="./@url"/>
            </xsl:attribute>
            <xsl:attribute name="width">
                <xsl:text>80%</xsl:text>
            </xsl:attribute>
        </img>
    </xsl:template>
    <xsl:template match="tei:anchor">
        <a>
            <xsl:attribute name="href">
                <xsl:value-of select="concat('#', ./@xml:id)"/>
            </xsl:attribute>
            <xsl:text>Haut de page</xsl:text>
        </a>
    </xsl:template>

    <!--<xsl:template match="text()">
        <xsl:value-of select="normalize-space(.)"/>
    </xsl:template>-->


    <!-- DEBUG SECTION - Templates used to show warnings if unexpected elements or attributes -->
    <xsl:template match="tei:*">
        <span class="btn-warning btn-sm" title="Élément non pris en charge par la XSLT.">
            <xsl:text>Élément "</xsl:text>
            <xsl:value-of select="name(.)"/>
            <xsl:text>" non pris en charge par la transformation XSL.</xsl:text>
        </span>
    </xsl:template>
    <!-- END DEBUG SECTION -->
</xsl:stylesheet>
