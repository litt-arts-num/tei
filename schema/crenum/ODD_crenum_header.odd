<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Manuel d'encodage pour les différents header du projet CreNum - Édition numérique de
          la <title>Chronique française</title> de Guillaume Cretin</title>
      </titleStmt>
      <publicationStmt>
        <p>ODD CreNum des listes de référence</p>
      </publicationStmt>
      <sourceDesc>
        <p>Information about the source</p>
      </sourceDesc>
    </fileDesc>
  </teiHeader>
  <text>
    <body>
      <div>
        <div>
          <!-- Descriptions communes aux trois fichiers -->
          <head>Informations générales sur la création des fichiers header</head>
          <p>Le projet CreNum comporte trois fichiers header distincts : <list>
              <item>Un fichier header_liste_personnes</item>
              <item>Un fichier header_manuscrits</item>
              <item>Un fichier facsimiles</item>
            </list> Les éléments utilisés pour ces trois fichiers ainsi que leurs attributs sont
            décrits dans le fichier ci-contre. Une autre documentation est proposée afin d'expliquer
            l'encodage des livres.<lb/>Les trois fichiers header possèdent un certains nombres
            d'éléments communs que nous allons détailler ci-dessous : </p>
          <specGrpRef target="#global"/>
        </div>
        <div>
          <!-- Description scpécifique au fichier -->
          <head>Fichier header comprenant les listes de personnes et les listes de relations</head>
          <p>Le fichier header_liste_personnes contient trois listes de personnes ainsi qu'une liste
            de relations existantes entre les membre de la famille royale. La création des listes de
            personnes a demandé plusieurs étapes de travail que nous explicitons ci-après.<list>
              <item>Création d'un tableau par livre lors de l'encodage des <gi>persName</gi>. Ces
                tableaux contiennent le nom dans le texte, le nom normalisé, l'identifiant, l'URL
                VIAF, l'URL biblissima et l'URL Wikidata.</item>
              <item>Extraction des données de Wikidata par Théo Roulet.</item>
              <item>Nettoyage des tableaux par Ellen Delvallée. Regroupement des données extraites
                de Wikidata par Théo Roulet avec les données déjà présentes dans les tableaux des
                livres I et II et nettoyage des données.</item>
              <item>Création semi-automatique des trois listes de personnes en fonction du
                  <att>type</att> à l'aide d'une formule de concatenation dans un tableau
                excel.</item>
              <item>Création manuelle de la liste de relations.</item>
            </list> Ce fichier permet de créer, sur le site <title>Chroniques rimées</title>, une
            page avec un index des personnages ainsi qu'une page avec différentes visualisations des
            généalogies des rois des Francs et du partage du royaume.</p>
          <div>
            <head>Éléments communs aux listes de personnes et de relations :</head>
            <specGrpRef target="#liste_personnes_relations"/>
          </div>
          <div>
            <head>Éléments présents dans les listes de personnes :</head>
            <specGrpRef target="#liste_personnes"/>
          </div>
          <div>
            <head>Éléments présents dans la liste de relations :</head>
            <figure>
              <figDesc>Ces éléments permettent de créer plusieurs visualisations sur la généalogies
                des rois des Francs mentionnés par Guillaume Cretin.</figDesc>
              <graphic url="img/genealogy.png"/>
            </figure>
            <specGrpRef target="#liste_relations"/>
          </div>
        </div>
        <div>
          <!-- Description scpécifique au fichier -->
          <head>Fichier header décrivant les différents manuscrits</head>
          <p>Le fichier header_manuscrits contient une description des manuscrits utilisés dans le
            projet CreNum. Ce fichier permettra de créer, sur le site <title>Chroniques
              rimées</title>, une page bibliographique décrivant chaque copie de la <title>Chronique
              française</title> de Guillaume Cretin.</p>
          <specGrpRef target="#manuscrit"/>
        </div>
        <div>
          <!-- Description scpécifique au fichier -->
          <head>Fichier header avec des informations sur les enluminures</head>
          <p>Le fichier header_facsimiles est généré à partir du manisfest disponible sur mandragore
            grâce au script jsonAnnotation2teiFacsimile.bash.</p>
          <specGrpRef target="#header_facsimile"/>
        </div>
      </div>
      <anchor xml:id="top"/>

      <!-- MODULES UTILISES -->
      <div>
        <schemaSpec ident="crenum" start="TEI" prefix="tei_" targetLang="fr" docLang="fr">
          <moduleRef key="tei"/>
          <moduleRef key="header"
            include="appInfo application change edition editionStmt encodingDesc extent fileDesc idno listChange profileDesc publicationStmt revisionDesc sourceDesc teiHeader titleStmt"/>
          <moduleRef key="core"
            include="author bibl date desc label listBibl name note p ref resp respStmt title"/>
          <moduleRef key="textstructure" include="body TEI text"/>
          <moduleRef key="transcr" include="facsimile surface zone"/>
          <moduleRef key="linking" include="standOff"/>
          <moduleRef key="msdescription"
            include="additional collation decoDesc dimensions handDesc history height layout layoutDesc locus msContents msDesc msIdentifier msItem objectDesc physDesc provenance repository rubric support supportDesc surrogates width"/>
          <moduleRef key="namesdates"
            include="birth death listPerson listRelation occupation person persName relation settlement"/>
          <specGrp xml:id="global">
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="TEI"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="teiHeader"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="fileDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="titleStmt"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="title_header"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="author"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="name"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="editionStmt"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="edition"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="publicationStmt"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="p_header"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="sourceDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="date_header"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="note_header"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="idno"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="text"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="body_header"/>
          </specGrp>
          <specGrp xml:id="liste_personnes_relations">
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="respStmt"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="resp"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="standOff"/>
          </specGrp>
          <specGrp xml:id="liste_personnes">
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="listPerson"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="person"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="persName_header"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="birth"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="death"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="occupation"/>
          </specGrp>
          <specGrp xml:id="liste_relations">
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="listRelation"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="relation"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="desc"/>
          </specGrp>
          <specGrp xml:id="manuscrit">
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="msDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="msIdentifier"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="settlement"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="repository"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="msContents"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="msItem"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="locus"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="rubric"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="physDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="objectDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="supportDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="support"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="extent"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="dimensions"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="height"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="width"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="collation"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="layoutDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="layout"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="handDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="decoDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="history"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="provenance"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="additional"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="surrogates"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="ref"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="listBibl"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="bibl"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="encodingDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="appInfo"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="application"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="label"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="profileDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="revisionDesc"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="listChange"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="change"/>
          </specGrp>
          <specGrp xml:id="header_facsimile">
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="facsimile"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="surface"/>
            <xi:include xmlns:xi="http://www.w3.org/2001/XInclude"
              href="ODD_crenum_commun.odd" parse="xml" xpointer="zone"/>
          </specGrp>
          <specGrpRef target="#global"/>
          <specGrpRef target="#liste_personnes_relations"/>
          <specGrpRef target="#liste_personnes"/>
          <specGrpRef target="#liste_relations"/>
          <specGrpRef target="#manuscrit"/>
          <specGrpRef target="#header_facsimile"/>
          <!-- DECLARATION DES CLASSES (SUPPRIMEES OU MODIFIEES) -->
          <classSpec type="atts" ident="att.anchoring" mode="change">
            <attList>
              <attDef ident="targetEnd" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.ascribed.directed" mode="change">
            <attList>
              <attDef ident="toWhom" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.canonical" mode="change">
            <attList>
              <attDef ident="key" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.ranging" mode="delete"/>
          <classSpec type="atts" ident="att.dimensions" mode="change">
            <attList>
              <attDef ident="quantity" mode="delete"/>
              <attDef ident="extent" mode="delete"/>
              <attDef ident="precision" mode="delete"/>
              <attDef ident="scope" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.written" mode="delete"/>
          <classSpec type="atts" ident="att.damaged" mode="change">
            <attList>
              <attDef ident="degree" mode="delete"/>
              <attDef ident="group" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.cReferencing" mode="delete"/>
          <classSpec type="atts" ident="att.datable" mode="change">
            <attList>
              <attDef ident="calendar" mode="delete"/>
              <attDef ident="period" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.datcat" mode="delete"/>
          <classSpec type="atts" ident="att.declaring" mode="delete"/>
          <classSpec type="atts" ident="att.global.responsibility" mode="change">
            <attList>
              <attDef ident="resp" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.editLike" mode="change">
            <attList>
              <attDef ident="evidence" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.global.rendition" mode="change">
            <attList>
              <attDef ident="rendition" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.global" mode="change">
            <attList>
              <attDef ident="xml:space" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.handFeatures" mode="delete"/>
          <classSpec type="atts" ident="att.internetMedia" mode="delete"/>
          <classSpec type="atts" ident="att.media" mode="delete"/>
          <classSpec type="atts" ident="att.resourced" mode="delete"/>
          <classSpec type="atts" ident="att.interpLike" mode="delete"/>
          <classSpec type="atts" ident="att.measurement" mode="delete"/>
          <classSpec type="atts" ident="att.naming" mode="change">
            <attList>
              <attDef ident="role" mode="delete"/>
              <attDef ident="nymRef" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.notated" mode="delete"/>
          <classSpec type="atts" ident="att.typed" mode="change">
            <attList>
              <attDef ident="subtype" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.pointing" mode="change">
            <attList>
              <attDef ident="evaluate" mode="delete"/>
              <attDef ident="targetLang" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.pointing.group" mode="change">
            <attList>
              <attDef ident="domains" mode="delete"/>
              <attDef ident="targFunc" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.scoping" mode="delete"/>
          <classSpec type="atts" ident="att.segLike" mode="change">
            <attList>
              <attDef ident="function" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.sortable" mode="delete"/>
          <classSpec type="atts" ident="att.edition" mode="delete"/>
          <classSpec type="atts" ident="att.styleDef" mode="delete"/>
          <classSpec type="atts" ident="att.timed" mode="delete"/>
          <classSpec type="atts" ident="att.transcriptional" mode="change">
            <attList>
              <attDef ident="cause" mode="delete"/>
              <attDef ident="seq" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.citing" mode="delete"/>
          <classSpec type="atts" ident="att.formula" mode="delete"/>
          <classSpec type="atts" ident="att.locatable" mode="delete"/>
          <classSpec type="atts" ident="att.partials" mode="delete"/>
          <classSpec type="atts" ident="att.duration.iso " mode="delete"/>
          <classSpec type="atts" ident="att.citeStructurePart" mode="delete"/>
          <classSpec type="atts" ident="att.patternReplacement" mode="delete"/>
          <classSpec type="atts" ident="att.milestoneUnit" mode="delete"/>
          <classSpec type="atts" ident="att.lexicographic.normalized" mode="change">
            <attList>
              <attDef ident="norm" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.linguistic" mode="delete"/>
          <classSpec type="atts" ident="att.global.analytic" mode="delete"/>
          <classSpec type="atts" ident="att.global.linking" mode="change">
            <attList>
              <attDef ident="select" mode="delete"/>
              <attDef ident="exclude" mode="delete"/>
              <attDef ident="copyOf" mode="delete"/>
              <attDef ident="sameAs" mode="delete"/>
              <attDef ident="synch" mode="delete"/>
            </attList>
          </classSpec>
          <classSpec type="atts" ident="att.datable.custom" mode="delete"/>
          <classSpec type="atts" ident="att.datable.iso" mode="delete"/>
          <classSpec type="atts" ident="att.rdgPart" mode="delete"/>
          <classSpec type="atts" ident="att.textCritical" mode="change">
            <attList>
              <attDef ident="cause" mode="delete"/>
              <attDef ident="varSeq" mode="delete"/>
              <attDef ident="require" mode="delete"/>
            </attList>
          </classSpec>
          <!--<classSpec type="atts" ident="att.personal" mode="delete"/>-->
          <!--<classSpec type="atts" ident="att.declarable" mode="delete"/>-->
          <!--<classSpec type="atts" ident="att.fragmentable" mode="delete"/>-->
          <!--<classSpec type="atts" ident="att.divLike" mode="delete"/>-->
          <!--<classSpec type="atts" ident="att.docStatus" mode="delete"/>-->
        </schemaSpec>
      </div>
    </body>
  </text>
</TEI>
