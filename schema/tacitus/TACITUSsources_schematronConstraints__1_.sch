<?xml version="1.0" encoding="UTF-8"?>
<sch:schema xmlns:sch="http://purl.oclc.org/dsdl/schematron" queryBinding="xslt2"
    xmlns:sqf="http://www.schematron-quickfix.com/validator/process"
    xmlns:tei="http://www.tei-c.org/ns/1.0">
    <sch:ns prefix="tei" uri="http://www.tei-c.org/ns/1.0"/>
    <sch:pattern id="floruitValues">
        <sch:rule context="tei:floruit">   
            <sch:assert test="(
                matches(@notBefore-custom, '^(I{0,3}|IV|V?I{0,3}|IX|X?I{0,3}|XIV) (a|p)\.C\.$') and
                matches(@notAfter-custom, '^(I{0,3}|IV|V?I{0,3}|IX|X?I{0,3}|XIV) (a|p)\.C\.$') 
                )">
                Les attributs 'notBefore-custom' et 'notAfter-custom' doivent contenir un chiffre romain valide suivi de 'a.C.' ou 'p.C.'.
            </sch:assert>
            <sch:assert test="((@notBefore-custom = @notAfter-custom and . = @notBefore-custom)
                or (@notBefore-custom != @notAfter-custom and . = string-join((@notBefore-custom, '-', @notAfter-custom))))">
                La valeur de l'élément 'floruit' doit correspondre soit à la valeur des attributs lorsqu'ils sont identiques, soit à l'union des deux valeurs des attributs lorsqu'ils sont différents divisés par un tiret.
                Exemple: II a.C.-I a.C.
            </sch:assert>
            
        </sch:rule>
    </sch:pattern>
</sch:schema>