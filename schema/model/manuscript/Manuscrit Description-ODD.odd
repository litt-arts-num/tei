<?xml version="1.0" encoding="UTF-8"?>
<TEI xmlns:xi="http://www.w3.org/2001/XInclude" xmlns:svg="http://www.w3.org/2000/svg"
  xmlns:math="http://www.w3.org/1998/Math/MathML" xmlns="http://www.tei-c.org/ns/1.0">
  <teiHeader>
    <fileDesc>
      <titleStmt>
        <title>Manuel d'encodage pour la description des sources manuscrites</title>
      </titleStmt>
      <publicationStmt>
        <p>Version du 9 juin 2023</p>
      </publicationStmt>
      <sourceDesc>
        <p>Manuel généré pour pouvoir repondre au besoin d'encodage des sources manuscrites.
          Modifié, complété et documenté par Serena Crespi.</p>
      </sourceDesc>
    </fileDesc>
  </teiHeader>
  <text>
    <body>

      <schemaSpec ident="oddex1" start="TEI">
        <moduleRef key="header"
          include="change listChange revisionDesc profileDesc encodingDesc extent idno sourceDesc publicationStmt edition editionStmt titleStmt fileDesc teiHeader"/>
        <moduleRef key="core" include="name listBibl bibl p date title author"/>
        <moduleRef key="tei"/>
        <moduleRef key="textstructure" include="TEI"/>
        <moduleRef key="msdescription "
          include="rubric additional provenance history decoDesc handDesc layout layoutDesc collation width height dimensions support supportDesc objectDesc physDesc locus msItem msContents repository msIdentifier msDesc"/>
        <moduleRef key="namesdates  " include="settlement"/>

        <elementSpec ident="TEI" mode="change" xml:id="TEI">
          <desc>Elément racine du fichier TEI.</desc>

        </elementSpec>
        <elementSpec ident="teiHeader" mode="change" xml:id="teiHeader">
          <desc>Le <gi>teiHeader</gi> contient des informations sur le fichier xml, le projet ainsi
            que des informations sur la pièce. C'est dans cette partie que sont renseignés les
            métadonnées.</desc>

        </elementSpec>
        <elementSpec ident="fileDesc" mode="change" xml:id="fileDesc">
          <desc>L'élément <gi>fileDesc</gi> contient une description bibliographique du fichier. </desc>

        </elementSpec>
        <elementSpec ident="titleStmt" mode="change" xml:id="titleStmt">
          <desc>L'élément <gi>titleStmt</gi> contient les informations sur le titre, les auteurs et
            responsables du fichier.</desc>

        </elementSpec>
        <elementSpec ident="title" mode="change" xml:id="title">
          <desc>L'élément <gi>title</gi> correspond au titre du fichier lorsqu'il se trouve dans
            l'élément <ref target="#titleStmt"><gi>titleStmt</gi></ref>. Dans la description des
            manuscrits, plus précisement dans l'élément <ref target="#msContents"
                ><gi>msContents</gi></ref>, il contient le titre donné aux différentes sections des
            manuscrits.</desc>

        </elementSpec>
        <elementSpec ident="author" mode="change" xml:id="author">
          <desc>L'élément <gi>author</gi> peut désigner deux choses différentes tout dépend s'il
            appartient à l'élément <ref target="#titleStmt"><gi>titleStmt</gi></ref> présent dans le
              <ref target="#teiHeader"><gi>teiHeader</gi></ref> de l'ensemble des fichiers header du
            projet ou bien à l'élément <ref target="#msItem"><gi>msItem</gi></ref> présent dans le
              <ref target="#msContents"><gi>msContents</gi></ref> (lui même présent dans la
            description du manuscrit). <list>
              <item>Au sein du <ref target="#titleStmt"><gi>titleStmt</gi></ref>, cet élément
                  <gi>author</gi> désigne les éditeurs de l'édition numérique.</item>
              <item>Dans le <ref target="#msItem"><gi>msItem</gi></ref> il désigne l'auteur de
                l'oeuvre contenue dans le manuscrit.</item>
            </list>
          </desc>
        </elementSpec>
        <elementSpec ident="editionStmt" mode="change" xml:id="editionStmt">
          <desc>L'élément <gi>editionStmt</gi> contient l'élément <ref target="#edition"
                ><gi>edition</gi></ref> qui permet de décrire l'édition du fichier.</desc>

        </elementSpec>
        <elementSpec ident="edition" mode="change" xml:id="edition">
          <desc>L'élément <gi>edition</gi> décrit les différents éléments d'édition d'un
            texte.</desc>

        </elementSpec>
        <elementSpec ident="date" mode="change" xml:id="date">
          <desc>L'élément <gi>date</gi> permet de mentionner les dates d'édition du fichier ou bien
            ses dates de modifications.</desc>

        </elementSpec>
        <elementSpec ident="publicationStmt" mode="change" xml:id="publicationStmt">
          <desc>L'élément <gi>publicationStmt</gi> contient les informations au sujet de la
            publication et de la diffusion d'un texte numérique.</desc>

        </elementSpec>
        <elementSpec ident="p" mode="change" xml:id="p">
          <desc>L'élément <gi>p</gi> correspond à un paragraphe.</desc>

        </elementSpec>
        <elementSpec ident="sourceDesc" mode="change" xml:id="sourceDesc">
          <desc type="todo"/>

        </elementSpec>
        <elementSpec ident="msDesc" mode="change" xml:id="msDesc">
          <desc>L'élément <gi>msDesc</gi> contient la description d'un manuscrit. Cet élément est
            complété par l'attribut <att>xml:id</att>qui fournit l'identifiant du manuscrit.</desc>
          <attList>
            <attDef ident="xml:id" mode="change">
              <desc type="todo"/>
            </attDef>
          </attList>
        </elementSpec>
        <elementSpec ident="msIdentifier" mode="change" xml:id="msIdentifier">
          <desc>L'élément <gi>msIdentifier</gi> contient les informations permettant d'identifier le
            manuscrit décrit.</desc>

        </elementSpec>
        <elementSpec ident="settlement" mode="change" xml:id="settlement">
          <desc>L'élément <gi>settlement</gi> indique la ville où se trouve le manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="repository" mode="change" xml:id="repository">
          <desc>L'élément <gi>repository</gi> indique la bibliothèque où se trouve le
            manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="idno" mode="change" xml:id="idno">
          <desc>L'élément <gi>idno</gi> permet de donner un identifiant standartisé au texte contenu
            dans l'élément.</desc>
        </elementSpec>
        <elementSpec ident="msContents" mode="change" xml:id="msContents">
          <desc>Cet élément contient la description de la structuration du manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="msItem" mode="change" xml:id="msItem">
          <desc>Contient les différents éléments structurants du manuscrit.</desc>



        </elementSpec>
        <elementSpec ident="locus" mode="change" xml:id="locus">
          <desc>L'élément <gi>locus</gi> peut venir utilisé pour indiquer la partie d'un
            manuscrit.</desc>
          <!--REVOIR-->

        </elementSpec>
        <elementSpec ident="physDesc" mode="change" xml:id="physDesc">
          <desc>L'élément <gi>physDesc</gi> contient la description physique complète d'un
            manuscrit. Il se compose notamment de l'élément <ref target="#objectDesc"
                ><gi>objectDesc</gi></ref> ainsi que de l'élément <ref target="#decoDesc"
                ><gi>decoDesc</gi></ref>.</desc>

        </elementSpec>
        <elementSpec ident="objectDesc" mode="change" xml:id="objectDesc">
          <desc>Contenu dans l'élément <ref target="#physDesc"><gi>physDesc</gi></ref>, cet élément
            contient la description matérielle du manuscrit en question.</desc>

        </elementSpec>
        <elementSpec ident="supportDesc" mode="change" xml:id="supportDesc">
          <desc>L'élément <gi>supportDesc</gi> permet de donner une description du support matériel
            du manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="support" mode="change" xml:id="support">
          <desc>L'élément <gi>supportDesc</gi> permet d'indiquer le support matériel dont le
            manuscrit est composé (ex. papier, parchemin etc.).</desc>

        </elementSpec>
        <elementSpec ident="extent" mode="change" xml:id="extent">
          <desc>L'élément <gi>extent</gi> permet d'exprimer la taille d'un manuscrit. Il contient
            notamment une explication du nombre de feuillets de garde, de feuillets non numérotés,
            de feuillets numérotés mais aussi l'élément <ref target="#dimensions"
                ><gi>dimensions</gi></ref>.</desc>

        </elementSpec>
        <elementSpec ident="dimensions" mode="change" xml:id="dimensions">
          <desc>L'élément <gi>dimensions</gi> permet de préciser les dimensions du manuscrits. Il
            contient les éléments <ref target="#height"><gi>height</gi></ref> et <ref
              target="#width"><gi>width</gi></ref></desc>
          <attList>
            <attDef ident="type" mode="change">
              <desc type="todo"/>
              <valList type="closed" mode="replace">
                <valItem ident="leaf"/>
              </valList>
            </attDef>
            <attDef ident="unit" mode="change">
              <desc type="todo"/>
              <valList type="closed" mode="replace">
                <valItem ident="mm"/>
              </valList>
            </attDef>
          </attList>
        </elementSpec>
        <elementSpec ident="height" mode="change" xml:id="height">
          <desc>Contenu dans l'élément <ref target="#dimensions"><gi>dimensions</gi></ref>, cet
            élément contient la hauteur de l'élément décrit.</desc>

        </elementSpec>
        <elementSpec ident="width" mode="change" xml:id="width">
          <desc>Contenu dans l'élément <ref target="#dimensions"><gi>dimensions</gi></ref>, cet
            élément contient la largeur de l'élément décrit.</desc>

        </elementSpec>
        <elementSpec ident="collation" mode="change" xml:id="collation">
          <desc>Contenu dans l'élément <ref target="#supportDesc"><gi>supportDesc</gi></ref>, cet
            élément permet de décrire l'organisation des feuillets ou bifeuillets d'un
            manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="layoutDesc" mode="change" xml:id="layoutDesc">
          <desc>L'élément <gi>layoutDesc</gi> permet de décrire la mise en page d'un
            manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="layout" mode="change" xml:id="layout">
          <desc>L'élément <gi>layout</gi> est contenu dans l'élément <ref target="#layoutDesc"
                ><gi>layoutDesc</gi></ref>. Cet élément permet de décrire la mise en page du texte
            au sein du manuscrit.</desc>
          <attList>
            <attDef ident="columns" mode="change">
              <desc>Cet attribut permet de mentionner le nombre de colonnes de texte présente sur le
                manuscrit.</desc>

            </attDef>
            <attDef ident="writtenLines" mode="change">
              <desc>Cet attribut permet d'indiquer le nombre de lignes écrites par colonnes.</desc>
            </attDef>
          </attList>
        </elementSpec>
        <elementSpec ident="handDesc" mode="change" xml:id="handDesc">
          <desc>L'élément <gi>handDesc</gi> permet de décrire les types d'écriture présentes dans un
            manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="decoDesc" mode="change" xml:id="decoDesc">
          <desc>Contenu dans <ref target="#physDesc"><gi>physDesc</gi></ref>, l'élément
              <gi>decoDesc</gi> permet de décrire la décoration du manuscrit. Il contient un ou
            plusieurs élément <ref target="#p"><gi>p</gi></ref>.</desc>

        </elementSpec>
        <elementSpec ident="history" mode="change" xml:id="history">
          <desc>L'élément <gi>history</gi> permet d'exprimer les éléments pouvant compléter
            l'historique du manuscrit. Il contient l'élément <ref target="#provenance"
                ><gi>provenance</gi></ref>.</desc>

        </elementSpec>
        <elementSpec ident="provenance" mode="change" xml:id="provenance">
          <desc>L'élément <gi>provenance</gi> permet de préciser des informations sur l'histoire du
            manuscrit.</desc>

        </elementSpec>
        <elementSpec ident="additional" mode="change" xml:id="additional">
          <desc>L'élément <gi>additional</gi> permet de donner des informations supplémentaires au
            sujet du manuscrit décrit. Il contient l'élément <ref target="#surrogates"
                ><gi>surrogates</gi></ref> qui permet de mentionner ses reproductions numériques ou
            photographiques. Il peut également contenir une bibliographie avec notament l'élément
              <ref target="#listBibl"><gi>listBibl</gi></ref>.</desc>

        </elementSpec>

        <!--   SI JAMAIS IL FAUT INSERER DES REPRODUCTIONS-->
        <!--   <elementSpec ident="surrogates" mode="change" xml:id="surrogates">
          <desc type="todo"/>
        </elementSpec>-->

        <elementSpec ident="rubric" mode="change" xml:id="rubric">
          <desc type="todo"/>

        </elementSpec>
        <elementSpec ident="listBibl" mode="change" xml:id="listBibl">
          <desc>L'élément <gi>listBibl</gi> permet de faire une liste de références
            bibliographiques. Cet élément contient un ou plusieurs éléments <ref target="#bibl"
                ><gi>bibl</gi></ref></desc>
        </elementSpec>

        <elementSpec ident="bibl" mode="change" xml:id="bibl">
          <desc>L'élément <gi>bibl</gi> contient une référence bibliographique. Il est présent au
            sein de l'élément <ref target="#listBibl"><gi>listBibl</gi></ref>.</desc>
        </elementSpec>

        <elementSpec ident="encodingDesc" mode="change" xml:id="encodingDesc">
          <desc>L'élément <gi>encodingDesc</gi> est utilisé pour déclarer des informations sur l'encodage utilisé.</desc>

        </elementSpec>
        <elementSpec ident="profileDesc" mode="change" xml:id="profileDesc">
          <desc>fournit une description détaillée des aspects non bibliographiques du texte, 
            notamment les langues utilisées et leurs variantes, les circonstances de sa production, les collaborateurs et leur statut.</desc>

        </elementSpec>
        <elementSpec ident="revisionDesc" mode="change" xml:id="revisionDesc">
          <desc>L'élément <gi>revisionDesc</gi> contient un aperçu des révisions du fichier.
            Le fichier permet d'avoir une liste des modifications apportées.</desc>

        </elementSpec>
        <elementSpec ident="listChange" mode="change" xml:id="listChange">
          <desc>L'élément <gi>listChange</gi> contenu dans le <ref target="#revisionDesc"><gi>revisionDesc</gi></ref> permet de renseigner une liste des modifications
            que le fichier a subi.</desc>
  
        </elementSpec>
        <elementSpec ident="change" mode="change" xml:id="change">
          <desc>Contenu dans l'élément <ref target="#listChange"><gi>listChange</gi></ref> de ce
            dernier, l'élément <gi>change</gi> permet d'indiquer une modification apportée au
            fichier. Il contient l'élément <ref target="#date"><gi>date</gi></ref> qui permet de
            dater la modification et <ref target="#name"><gi>name</gi></ref> qui donne le nom de
            la personne à l'origine de cette modification.</desc>

        </elementSpec>
        <elementSpec ident="name" mode="change" xml:id="name">
          <desc>Présent dans les <ref target="#teiHeader"><gi>teiHeader</gi></ref>, l'élément
              <gi>name</gi> contient le nom de la personne responsable du contenu du fichier et de
            l'encodage ou de la modification effectuée.</desc>

        </elementSpec>


      </schemaSpec>
    </body>
  </text>
</TEI>
